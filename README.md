**Zokrates Installation**
Using Docker is currently the recommended way to get started with Zokrates.
docker run -ti zokrates/zokrates /bin/bash

Or build yourself with the following commands:
git clone https://github.com/JacobEberhardt/ZoKrates
cd ZoKrates
docker build -t zokrates .
docker run -ti zokrates /bin/bash
cd ZoKrates/target/release


**now for the intresting part:**
now you need to create file
touch calcMathOperation.code

copy the data from this repository to the file with vi editor
vi calcMathOperation.code

now you need to compile the file
./zokrates compile -i 'calcMathOperation.code'

now you need to setup
./zokrates setup

and now you run the code with 3 parameters
./zokrates compute-witness -a 1 2 3
1 - represent the start number
2 - represent the final number
3 - represent the operation*

*the operation is a number that build in the following order
first number represnt the operation:
1- add 1
2- sub 2
3- mul 2
4- div 2
second number repesent if the operation is legal
1- illegal
2- legal

after you run this you will get 2 option
1. Witness if you enter all correct
2. you will get an error if one of the args are bad

for genaret the proof and export the verifier use this command
./zokrates generate-proof
./zokrates export-verifier


**Important points**
The size of the third parameter thet represent the operation is defined in the start of the main mathod because today there is no way to
create a loop that get a dynamic number for iteration. 
if you want to change pay attention to change the var numberOfOperation and the for loop.

